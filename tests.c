#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "CUnit.h"
#include "Automated.h"
#include "calculate.h"


/* Runs a test input that *should* pass and produce an int result
 */
void test01(void) {
  int expected = 100;
  int actual = calculate(13000,0,1);
  CU_ASSERT_EQUAL( expected , actual );
}

void test02(void){
  int expected = 0;
  int actual = calculate(1000, 0, 0);
  CU_ASSERT_EQUAL(expected, actual);
}

void test03(void){
  int expected = 0;
  int actual = calculate(10000, 1, 0);
CU_ASSERT_EQUAL(expected, actual);
}

void test04(void){
  int expected = 12000;
  int actual = calculate(100000, 0, 1);
  CU_ASSERT_EQUAL(expected, actual);
}

void test05(void){
  int expected = 0;
  int actual = calculate(5, -1, 1);
  CU_ASSERT_EQUAL(expected, actual);
}

void test06(void){
  int expected = 0;
  int actual = calculate(5, 1, -1);
  CU_ASSERT_EQUAL(expected, actual);
}
/* The main() function for setting up and running the tests.
 * Returns a CUE_SUCCESS on successful running, another
 * CUnit error code on failure.
 */
int main()
{
   CU_pSuite Suite = NULL;

   /* initialize the CUnit test registry */
   if (CUE_SUCCESS != CU_initialize_registry()) { return CU_get_error(); }

   /* add a suite to the registry */
   Suite = CU_add_suite("Suite_of_tests_with_valid_inputs", NULL, NULL);
   if (NULL == Suite) {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* add the tests to Suite */
   if (
          (NULL == CU_add_test(Suite, "13000/0/1", test01))
      ||  (NULL == CU_add_test(Suite, "1000/0/0", test02))
      ||  (NULL == CU_add_test(Suite, "10000/1/0", test03))
      ||  (NULL == CU_add_test(Suite, "100000/0/1", test04))
      ||  (NULL == CU_add_test(Suite, "5/-1/1", test05)) 
      ||  (NULL == CU_add_test(Suite, "5/1/-1", test06))
    )
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* Run all tests using automated interface, with output to 'test-Results.xml' */
    CU_set_output_filename("test");

   CU_automated_run_tests();

   CU_cleanup_registry();
   return CU_get_error();
}
